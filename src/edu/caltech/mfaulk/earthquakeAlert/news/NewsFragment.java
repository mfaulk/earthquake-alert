package edu.caltech.mfaulk.earthquakeUltra.news;

import java.text.DateFormat;
import java.util.Date;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import edu.caltech.mfaulk.earthquakeUltra.R;
import edu.caltech.mfaulk.earthquakeUltra.SharedState;
import edu.caltech.mfaulk.earthquakeUltra.data.NewsDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.NewsProvider;

public class NewsFragment extends ListFragment implements
		LoaderManager.LoaderCallbacks<Cursor> {
	private static final String DEBUG_TAG = "NewsFragment";
	private static final int NEWS_LIST_LOADER = 0x01;

	//private ProgressDialog pDialog;

	private SimpleCursorAdapter adapter;

	public interface OnNewsSelectedListener {
		public void onNewsSelected(String newsUrl);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String[] uiBindFrom = { NewsDatabase.COL_URL, NewsDatabase.COL_TITLE,
				NewsDatabase.COL_DATE, NewsDatabase.COL_DESC };
		int[] uiBindTo = { R.id.news_page_url, R.id.news_title,
				R.id.news_pub_date, R.id.news_description };

		getLoaderManager().initLoader(NEWS_LIST_LOADER, null, this);
		adapter = new SimpleCursorAdapter(
				getActivity().getApplicationContext(),
				R.layout.news_item_list_row, null, uiBindFrom, uiBindTo,
				CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		adapter.setViewBinder(new NewsViewBinder());
		
		setListAdapter(adapter);
	}

	private class NewsViewBinder implements SimpleCursorAdapter.ViewBinder {
	    @Override
	    public boolean setViewValue(View view, Cursor cursor, int index) {
	        if (index == cursor.getColumnIndex(NewsDatabase.COL_DATE)) {
	            // get a locale based string for the date
	            DateFormat formatter = android.text.format.DateFormat
	                    .getDateFormat(getActivity().getApplicationContext());
	            long date = cursor.getLong(index);
	            Date dateObj = new Date(date * 1000);
	            ((TextView) view).setText(formatter.format(dateObj));
	            return true;
	        } else {
	            return false;
	        }
	    }
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Log.d(DEBUG_TAG, "Clicked position: " + Integer.toString(position));
		Cursor c = ((SimpleCursorAdapter) l.getAdapter()).getCursor();
		c.moveToPosition(position);
		String newsUrl = c.getString(1);
		Log.d(DEBUG_TAG, "Selected url: " + newsUrl);
		SharedState sharedState = SharedState.getInstance();
		sharedState.setNewsUrl(newsUrl);
		NewsActivity parentActivity = (NewsActivity) getActivity();
		parentActivity.onNewsSelected(newsUrl);
		//((TabActivity) getActivity().getParent()).getTabHost().setCurrentTab(2);
	}
	
	
	
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		String[] projection = { NewsDatabase.ID, NewsDatabase.COL_URL,
				NewsDatabase.COL_TITLE, NewsDatabase.COL_DATE,
				NewsDatabase.COL_DESC };
		CursorLoader cursorLoader = new CursorLoader(getActivity(),
				NewsProvider.CONTENT_URI, projection, null, null, NewsDatabase.COL_DATE+" desc");
		return cursorLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		// TODO: check this
		adapter.swapCursor(null);
	}

}