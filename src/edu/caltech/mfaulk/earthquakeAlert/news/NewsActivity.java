package edu.caltech.mfaulk.earthquakeUltra.news;

import supportExtras.WebViewFragment;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import edu.caltech.mfaulk.earthquakeUltra.R;
import edu.caltech.mfaulk.earthquakeUltra.service.NewsDownloaderService;

public class NewsActivity extends Activity implements
		NewsFragment.OnNewsSelectedListener {

	private static final String DEBUG_TAG = "NewsActivity";
	// TODO: put this somewhere, maybe res/values/strings?
	public static final String GOOGLE_NEWS_FEED = "http://news.google.com/news?q=earthquake+magnitude&output=rss";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.empty_linear_layout);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		Fragment fragment = new NewsFragment();
		fragmentTransaction.add(R.id.myFragment, fragment);
		fragmentTransaction.commit();
	}

	@Override
	public void onNewsSelected(String newsUrl) {
		Log.d(DEBUG_TAG, "onNewsSelected: " + newsUrl);
		WebViewFragment webViewFragment = new WebViewFragment();
		webViewFragment.setUrl(newsUrl);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.myFragment, webViewFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	@Override
	public boolean onPreparePanel(int featureId, View view, Menu menu) {
		// TODO should this be in onCreatePanel?
		Log.d(DEBUG_TAG, "onPreparePanelMenu");
		Intent intent = new Intent(this.getApplicationContext(),
				NewsDownloaderService.class);
		intent.setData(Uri.parse(GOOGLE_NEWS_FEED));

		MenuItem refresh = menu.findItem(R.id.refresh_option_item);
		refresh.setIntent(intent);

		return super.onPreparePanel(featureId, view, menu);
	}

	@Override
	public boolean onCreatePanelMenu(int arg0, Menu arg1) {
		// TODO Auto-generated method stub
		Log.d(DEBUG_TAG, "onCreatePanelMenu");
		return super.onCreatePanelMenu(arg0, arg1);
	}

}
