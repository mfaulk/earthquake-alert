package edu.caltech.mfaulk.earthquakeUltra.news;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import edu.caltech.mfaulk.earthquakeUltra.R;
 
public class DisplayWebPageActivity extends Activity {
	private static final String TAG = "DisplayWebPageActivity";
    WebView webview;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
 
        Intent in = getIntent();
        String page_url = in.getStringExtra("page_url");
        Log.d(TAG, "Displaying page: " + page_url );
        webview = (WebView) findViewById(R.id.webpage);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(page_url);
 
        webview.setWebViewClient(new DisplayWebPageActivityClient());
    }
 
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
 
    private class DisplayWebPageActivityClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            // NOTE: this was true originally
            return false;
        }
    }
 
}