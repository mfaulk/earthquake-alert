package edu.caltech.mfaulk.earthquakeUltra.map;

import java.util.HashMap;
import java.util.Map;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import edu.caltech.mfaulk.earthquakeUltra.SharedState;
import edu.caltech.mfaulk.earthquakeUltra.data.EventDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.EventProvider;


/**
 * Display marker for each quake.
 * 
 * @author mfaulk
 * 
 */
public class EventsMapFragment extends MapFragment implements
		LoaderManager.LoaderCallbacks<Cursor> {
	private static final String DEBUG_TAG = "EventsMapFragment";
	private GoogleMap mMap;
	private static final int EVENT_LIST_LOADER = 0x01;
	Map<Long, Marker> markers;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		markers = new HashMap<Long, Marker>();
		getLoaderManager().initLoader(EVENT_LIST_LOADER, null, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		Log.d(DEBUG_TAG, "onCreateView");
		mMap = getMap();
		if (mMap != null) {
			mMap.setTrafficEnabled(true);
			mMap.getUiSettings().setZoomControlsEnabled(true);
			mMap.getUiSettings().setCompassEnabled(true);
			mMap.getUiSettings().setMyLocationButtonEnabled(true);
		}
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		SharedState sharedState = SharedState.getInstance();
		Long quakeID = sharedState.getQuakeID();
		if (quakeID != null) {
			// center on the marker for this quake

			String[] projection = { EventDatabase.ID,
					EventDatabase.COL_QUAKE_TITLE, EventDatabase.COL_QUAKE_DESC,
					EventDatabase.COL_QUAKE_LAT, EventDatabase.COL_QUAKE_LON,
					EventDatabase.COL_QUAKE_MAG, EventDatabase.COL_QUAKE_DATE };
			
			Uri uri = ContentUris.withAppendedId(EventProvider.CONTENT_URI,
					quakeID);

			Cursor cursor = getActivity().managedQuery(uri, projection, null,
					null, null);
			if (cursor.moveToFirst()) {
				double lat = cursor.getDouble(3);
				double lon = cursor.getDouble(4);
				LatLng eventOrigin = new LatLng(lat, lon);
				mMap.moveCamera(CameraUpdateFactory.newLatLng(eventOrigin));
				// TODO: call showInfoWindow() on the corresponding Marker
				Marker marker = markers.get(quakeID);
				if (marker != null) {
					marker.showInfoWindow();
					sharedState.setQuakeID(null);
				} 
			} 
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		String[] projection = { EventDatabase.ID,
				EventDatabase.COL_QUAKE_TITLE, EventDatabase.COL_QUAKE_DESC,
				EventDatabase.COL_QUAKE_LAT, EventDatabase.COL_QUAKE_LON,
				EventDatabase.COL_QUAKE_MAG, EventDatabase.COL_QUAKE_DATE };
		CursorLoader cursorLoader = new CursorLoader(getActivity(),
				EventProvider.CONTENT_URI, projection, null, null, null);
		return cursorLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mMap.clear();
		markers.clear();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			// skip the ID at position 0
			long id = cursor.getLong(0);
			String title = cursor.getString(1);
			// String description = cursor.getString(2);
			Double lat = cursor.getDouble(3);
			Double lon = cursor.getDouble(4);
			Double mag = cursor.getDouble(5);
			String date = cursor.getString(6);
			MarkerOptions markerOptions = createMarker(title, lat, lon, mag, date);
			Marker marker = mMap.addMarker(markerOptions);
			markers.put(id, marker);
			cursor.moveToNext();
		}
		cursor.close();
		SharedState sharedState = SharedState.getInstance();
		Long quakeID = sharedState.getQuakeID();
		if (quakeID != null) {
			Marker marker = markers.get(quakeID);
			if (marker != null) {
				marker.showInfoWindow();
				sharedState.setQuakeID(null);
			} 
		}
	}

	private MarkerOptions createMarker(String title, double lat, double lon,
			double mag, String date) {
		MarkerOptions markerOptions = new MarkerOptions()
				.position(new LatLng(lat, lon)).title(title).snippet(date);
		if (mag < 3.0) {
			markerOptions.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
		} else if (mag < 4.5) {
			markerOptions.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
		} else {
			markerOptions.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_RED));
		}
		return markerOptions;
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub

	}

}
