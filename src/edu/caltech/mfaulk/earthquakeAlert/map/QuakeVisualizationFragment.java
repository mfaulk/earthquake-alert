package edu.caltech.mfaulk.earthquakeUltra.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import edu.caltech.mfaulk.earthquakeUltra.R;
import edu.caltech.mfaulk.earthquakeUltra.SharedState;
import edu.caltech.mfaulk.earthquakeUltra.data.CsnDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.CsnProvider;

public class QuakeVisualizationFragment extends MapFragment {
	private final String DEBUG_TAG = "QuakeVisualizationFragment";
	private GoogleMap mMap;
	private ProgressBar mProgressBar;
	// Sorted from earliest to latest
	private LinkedList<Pick> picks;
	private LinkedList<PickTombstone> pickTombstones;
	private Map<Pick, Marker> pickToMarker;
	private LatLng eventOrigin;
	private Quake eventQuake;
	private Handler handler = new Handler();
	private double now = 0.0;
	private final long delayMillis = 25;
	private final double pickMarkerDurationSeconds = 2.0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		picks = new LinkedList<Pick>();
		pickTombstones = new LinkedList<PickTombstone>();
		pickToMarker = new HashMap<Pick, Marker>();
		mProgressBar = (ProgressBar) getActivity().findViewById(
				R.id.vis_progressBar);
	}

	@Override
	public void onResume() {
		super.onResume();
		SharedState sharedState = SharedState.getInstance();
		Long quakeID = sharedState.getVisualizationID();
		if (quakeID != null) {
			Quake quake = getQuakeByID(quakeID);
			eventQuake = quake;
			Log.d(DEBUG_TAG, "Retrieved quake: ");
			Log.d(DEBUG_TAG,
					" --- magnitude: " + Double.toString(quake.magnitude));
			Log.d(DEBUG_TAG,
					" --- lat: " + Double.toString(quake.position.latitude));
			Log.d(DEBUG_TAG,
					" --- lon: " + Double.toString(quake.position.longitude));
			Log.d(DEBUG_TAG,
					" --- seconds: "
							+ Double.toString(quake.date.getMillis() / 1000));
			eventOrigin = quake.position;
			picks.addAll(getPicksByQuakeID(quakeID));
		}
		resetAnimation();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		Log.d(DEBUG_TAG, "onCreateView");
		mMap = getMap();
		if (mMap != null) {
			mMap.setTrafficEnabled(false);
			mMap.getUiSettings().setMyLocationButtonEnabled(true);
			mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
		}
		return view;
	}

	private void resetAnimation() {
		mMap.clear();
		pickTombstones.clear();
		pickToMarker.clear();
		handler.removeCallbacks(animationRunnable);
		now = 0.0;
		handler.postDelayed(animationRunnable, 100);
		// LatLng eventOrigin = new LatLng(34.13606, -118.12761);
		mMap.moveCamera(CameraUpdateFactory.newLatLng(eventOrigin));

		CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
		mMap.animateCamera(zoom);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		handler.removeCallbacks(animationRunnable);
	}

	private Runnable animationRunnable = new Runnable() {
		public void run() {

			//Log.d("animationRunnable", "run");
			now += delayMillis / 1000.0;

			if (!picks.isEmpty()) {
				double timeTotal = picks.getLast().secondsSinceEventOrigin;
				updateProgress(now, timeTotal);
			} else {
				Log.d(DEBUG_TAG, "picks is empty.");
			}
			while (picks.peek() != null
					&& picks.peek().secondsSinceEventOrigin < now) {

				Pick p = picks.poll();
				//Log.d("animationRunnable", "drawing a pick");
				if (!pickToMarker.containsKey(p)) {
					Marker marker = mMap.addMarker(new MarkerOptions()
							.position(p.position));
					pickToMarker.put(p, marker);
					pickTombstones.add(new PickTombstone(p,
							pickMarkerDurationSeconds));
				}

			}
			// clear stale pick markers
			while (pickTombstones.peek() != null
					&& pickTombstones.peek().removeAfterSecondsSinceOrigin < now) {
				Pick p = pickTombstones.poll().pick;
				Marker marker = pickToMarker.get(p);
				marker.setVisible(false);
				pickToMarker.remove(p);
			}

			if (picks.peek() != null || pickTombstones.peek() != null) {
				// reschedule this to produce an animation
				handler.postDelayed(this, delayMillis);
			}
		}
	};

	public void updateProgress(double timePassed, double timeTotal) {
		//Log.d(DEBUG_TAG, "timePassed: " + Double.toString(timePassed));
		//Log.d(DEBUG_TAG, "timeTotal: " + Double.toString(timeTotal));
		if (null != mProgressBar) {
			// Ignore rounding error here
			int progress = (int) (mProgressBar.getMax() * timePassed / timeTotal);
			//Log.d(DEBUG_TAG, "progress: " + Integer.toString(progress));
			mProgressBar.setProgress(progress);
		}
	}

	private class Pick implements Comparable<Pick> {
		public final LatLng position;
		// A value between 0 and 1.
		public final double intensity;
		// What format for time? I'd like this to be a Date object
		public final double secondsSinceEventOrigin;

		public Pick(LatLng position, double secondsSinceEventOrigin,
				double intensity) {
			this.position = position;
			this.secondsSinceEventOrigin = secondsSinceEventOrigin;
			this.intensity = intensity;
		}

		@Override
		public int compareTo(Pick another) {
			int retVal = 0;
			if (secondsSinceEventOrigin < another.secondsSinceEventOrigin) {
				retVal = -1;
			} else if (secondsSinceEventOrigin > another.secondsSinceEventOrigin) {
				retVal = 1;
			}
			return retVal;
		}
	}

	private class Quake {
		public final String title;
		public final String description;
		public final LatLng position;
		public final double magnitude;
		public final DateTime date;

		public Quake(String title, String description, double latitude,
				double longitude, double magnitude, Long dateMillis) {
			this.title = title;
			this.description = description;
			position = new LatLng(latitude, longitude);
			this.magnitude = magnitude;
			date = new DateTime(dateMillis);
		}
	}

	private class PickTombstone implements Comparable<PickTombstone> {
		public final Pick pick;
		public final double removeAfterSecondsSinceOrigin;

		public PickTombstone(Pick pick, double pickDuration) {
			this.pick = pick;
			this.removeAfterSecondsSinceOrigin = pick.secondsSinceEventOrigin
					+ pickDuration;
		}

		@Override
		public int compareTo(PickTombstone another) {
			int retVal = 0;
			if (removeAfterSecondsSinceOrigin < another.removeAfterSecondsSinceOrigin) {
				retVal = -1;
			} else if (removeAfterSecondsSinceOrigin > another.removeAfterSecondsSinceOrigin) {
				retVal = 1;
			}
			return retVal;
		}
	}

	private Quake getQuakeByID(long quakeID) {
		Quake quake = null;
		String[] projection = { CsnDatabase.COL_QUAKE_ID,
				CsnDatabase.COL_QUAKE_TITLE, CsnDatabase.COL_QUAKE_DESC,
				CsnDatabase.COL_QUAKE_LAT, CsnDatabase.COL_QUAKE_LON,
				CsnDatabase.COL_QUAKE_MAG, CsnDatabase.COL_QUAKE_DATE };

		Uri uri = ContentUris.withAppendedId(CsnProvider.EVENTS_URI, quakeID);
		// TODO: managedQuery is deprecated. Use cursorloader
		Cursor cursor = getActivity().managedQuery(uri, projection, null, null,
				null);
		if (cursor.moveToFirst()) {
			String title = cursor.getString(1);
			String description = cursor.getString(2);
			double lat = cursor.getDouble(3);
			double lon = cursor.getDouble(4);
			double magnitude = cursor.getDouble(5);
			long dateSeconds = cursor.getLong(6);
			quake = new Quake(title, description, lat, lon, magnitude,
					dateSeconds * 1000);
		}
		return quake;
	}

	private List<Pick> getPicksByQuakeID(long quakeID) {
		List<Pick> picks = new ArrayList<Pick>();
		String[] projection = { CsnDatabase.COL_PICK_ID,
				CsnDatabase.COL_PICK_DATE, CsnDatabase.COL_PICK_LAT,
				CsnDatabase.COL_PICK_LON, CsnDatabase.COL_PICK_MAG };

		String selection = CsnDatabase.COL_QUAKE_FOREIGN + "=?";
		String[] selectionArgs = { Long.toString(quakeID) };

		Uri uri = CsnProvider.PICKS_URI;

		Cursor cursor = getActivity().managedQuery(uri, projection, selection,
				selectionArgs, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			String[] columnNames = cursor.getColumnNames();
			for (String name : columnNames) {
				Log.d(DEBUG_TAG, "Column name: " + name);
			}
			long pickSeconds = cursor.getLong(1);
			double lat = cursor.getDouble(2);
			double lon = cursor.getDouble(3);
			LatLng position = new LatLng(lat, lon);
			double intensity = cursor.getDouble(4);
			double secondsSinceEventOrigin = pickSeconds
					- (eventQuake.date.getMillis() / 1000);
			Log.d(DEBUG_TAG, "Retrieved pick:");
			Log.d(DEBUG_TAG, " --- lat: " + Double.toString(lat));
			Log.d(DEBUG_TAG, " --- lon: " + Double.toString(lon));
			Log.d(DEBUG_TAG, " --- intensity: " + Double.toString(intensity));
			Log.d(DEBUG_TAG,
					" --- delay: " + Double.toString(secondsSinceEventOrigin));
			Pick p = new Pick(position, secondsSinceEventOrigin, intensity);
			picks.add(p);

			cursor.moveToNext();
		}
		Collections.sort(picks);
		return picks;
	}

}
