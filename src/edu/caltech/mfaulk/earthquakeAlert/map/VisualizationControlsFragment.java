package edu.caltech.mfaulk.earthquakeUltra.map;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import edu.caltech.mfaulk.earthquakeUltra.R;

/**
 * Provides multimedia controls for QuakeVisualizationFragment
 * @author mfaulk
 *
 */
public class VisualizationControlsFragment extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.visualization_controls, container, false);
	}
	
}
