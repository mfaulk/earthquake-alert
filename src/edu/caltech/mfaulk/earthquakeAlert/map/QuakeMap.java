package edu.caltech.mfaulk.earthquakeUltra.map;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import edu.caltech.mfaulk.earthquakeUltra.R;
import edu.caltech.mfaulk.earthquakeUltra.SharedState;

/**
 * NOTE: For some reason, the google-play-services_lib project must be open for
 * this to compile.
 * 
 * @author mfaulk
 * 
 */
public class QuakeMap extends Activity {
	private static final String DEBUG_TAG = "QuakeMap";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		setContentView(R.layout.empty_linear_layout);
//		FragmentManager fragmentManager = getSupportFragmentManager();
//		FragmentTransaction fragmentTransaction = fragmentManager
//				.beginTransaction();
//		
//		Fragment eventsFragment = new EventsMapFragment();
//		fragmentTransaction.add(R.id.myFragment, eventsFragment);
//		fragmentTransaction.commit();
	}

	@Override
	protected void onResume() {
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		
		SharedState sharedState = SharedState.getInstance();
		Long visualizationID = sharedState.getVisualizationID();
		if (visualizationID != null) {
			setContentView(R.layout.visualization_layout);
			Log.d(DEBUG_TAG, "adding VisualizationControlsFragment");
			Fragment visualizationFragment = new QuakeVisualizationFragment();
			fragmentTransaction.replace(R.id.myFragment, visualizationFragment);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
			
		} else {
			Log.d(DEBUG_TAG, "adding EventsMapFragment");
			setContentView(R.layout.empty_linear_layout);
			Fragment eventsFragment = new EventsMapFragment();
			fragmentTransaction.replace(R.id.myFragment, eventsFragment);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
		}
		super.onResume();
	}

}
