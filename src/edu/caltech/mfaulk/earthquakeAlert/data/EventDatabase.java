package edu.caltech.mfaulk.earthquakeUltra.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class EventDatabase extends SQLiteOpenHelper {

	public static final String DEBUG_TAG = "EventDatabase";
	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "event_data";

	// Schema: Quakes
	public static final String TABLE_QUAKES = "quakes";
	public static final String ID = "_id";
	public static final String COL_QUAKE_TITLE = "title";
	public static final String COL_QUAKE_DESC = "description";
	public static final String COL_QUAKE_MAG = "mag";
	public static final String COL_QUAKE_URL = "url";
	// Unix time in seconds
	public static final String COL_QUAKE_DATE = "date";
	//TODO: specify lat/lon convention
	public static final String COL_QUAKE_LAT = "lat";
	public static final String COL_QUAKE_LON = "lon";
	public static final String COL_QUAKE_NET = "network";

	private static final String CREATE_TABLE_QUAKES = "create table "
			+ TABLE_QUAKES + " (" + ID + " integer primary key autoincrement, "
			+ COL_QUAKE_NET + " text not null, "
			+ COL_QUAKE_URL	+ " text unique not null, " 
			+ COL_QUAKE_TITLE + " text, "
			+ COL_QUAKE_MAG + " real, " 
			+ COL_QUAKE_DESC + " text, " 
			+ COL_QUAKE_LAT + " real, "
			+ COL_QUAKE_LON + " real, " 
			+ COL_QUAKE_DATE+ " INTEGER NOT NULL "
			+");";
	private static final String DB_SCHEMA = CREATE_TABLE_QUAKES;
	
	public EventDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DB_SCHEMA);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DEBUG_TAG, "Upgrading database. Existing contents will be lost. ["
	            + oldVersion + "]->[" + newVersion + "]");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUAKES);
	    onCreate(db);
	}

}
