package edu.caltech.mfaulk.earthquakeUltra.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Based on http://mobile.tutsplus.com/tutorials/android/android-sdk_content-providers/
 * @author mfaulk
 *
 */
public class NewsDatabase extends SQLiteOpenHelper {
	public static final String DEBUG_TAG = "NewsDatabase";
	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "news_data";
	// Schema
	public static final String TABLE_NEWS = "news";
	public static final String ID = "_id";
	// maybe this should be an enum?
	public static final String COL_TITLE = "title";
	public static final String COL_URL = "url";
	public static final String COL_DESC = "description";
	public static final String COL_DATE = "date";

	private static final String CREATE_TABLE_NEWS = "create table "
			+ TABLE_NEWS + " (" + ID + " integer primary key autoincrement, "
			+ COL_TITLE + " text not null, " + COL_URL + " text unique not null, "
			+ COL_DESC + " text not null, " + COL_DATE + " INTEGER NOT NULL DEFAULT (strftime('%s','now'))"
			+");";
	private static final String DB_SCHEMA = CREATE_TABLE_NEWS;

	public NewsDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DB_SCHEMA);
		//seedData(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DEBUG_TAG, "Upgrading database. Existing contents will be lost. ["
	            + oldVersion + "]->[" + newVersion + "]");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS);
	    onCreate(db);
	}
	
	 /**
     * Create sample data to use
     * 
     * @param db
     *            The open database
     */
//    private void seedData(SQLiteDatabase db) {
//    	db.execSQL("insert into news (title, url, description, date) values ('The First Title', 'http://mobile.tutsplus.com/articles/news/best-of-tuts-in-february-2011/', 'The Description', (strftime('%s', '2011-02-01')));");
//    	db.execSQL("insert into news (title, url, description, date) values ('The Second Title', 'http://mobile.tutsplus.com/articles/news/best-of-tuts-in-february-2011/', 'The Description', (strftime('%s', '2011-02-01')));");
//    }

}
