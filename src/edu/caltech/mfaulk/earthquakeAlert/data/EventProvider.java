package edu.caltech.mfaulk.earthquakeUltra.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class EventProvider extends ContentProvider{
	private static final String DEBUG_TAG = "EventProvider";
	private static final String AUTHORITY = "edu.caltech.mfaulk.earthquakeUltra.data.EventProvider";
	
	private static final String EVENTS_BASE_PATH = "events";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + EVENTS_BASE_PATH);

	// Codes returned by UriMatcher
	public static final int QUAKES = 100;
	public static final int QUAKES_BY_ID = 110;

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, EVENTS_BASE_PATH, QUAKES);
		sURIMatcher.addURI(AUTHORITY, EVENTS_BASE_PATH + "/#", QUAKES_BY_ID);
	}
	
	// MIME Types
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/quake-events";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/quake-events";
	
	private EventDatabase mDB;
	
	@Override
	public boolean onCreate() {
		mDB = new EventDatabase(getContext());
		return true;
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
		case QUAKES:
			rowsAffected = sqlDB.delete(EventDatabase.TABLE_QUAKES, selection,
					selectionArgs);
			break;
		case QUAKES_BY_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(EventDatabase.TABLE_QUAKES,
						EventDatabase.ID + "=" + id, null);
			} else {
				rowsAffected = sqlDB.delete(EventDatabase.TABLE_QUAKES, selection
						+ " and " + EventDatabase.ID + "=" + id, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case QUAKES:
			return CONTENT_TYPE;
		case QUAKES_BY_ID:
			return CONTENT_ITEM_TYPE;
		default:
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Log.d(DEBUG_TAG, "INSERT");
		int uriType = sURIMatcher.match(uri);
		if (uriType != QUAKES) {
			throw new IllegalArgumentException("Invalid URI for insert");
		}
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		try {
			long newID = sqlDB.insertOrThrow(EventDatabase.TABLE_QUAKES, null,
					values);
			if (newID > 0) {
				Uri newUri = ContentUris.withAppendedId(uri, newID);
				getContext().getContentResolver().notifyChange(uri, null);
				return newUri;
			} else {
				throw new SQLException("Failed to insert row into " + uri);
			}
		} catch (SQLiteConstraintException e) {
			Log.i(DEBUG_TAG, "Ignoring constraint failure.");
		}
		return null;
	}

	

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(EventDatabase.TABLE_QUAKES);
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case QUAKES_BY_ID:
			queryBuilder.appendWhere(EventDatabase.ID + "="
					+ uri.getLastPathSegment());
			break;
		case QUAKES:
			// no extra filter
			break;
		default:
			throw new IllegalArgumentException("Unknown URI");
		}
		Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
				projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();

		int rowsAffected;

		switch (uriType) {
		case QUAKES_BY_ID:
			String id = uri.getLastPathSegment();
			StringBuilder modSelection = new StringBuilder(EventDatabase.ID
					+ "=" + id);

			if (!TextUtils.isEmpty(selection)) {
				modSelection.append(" AND " + selection);
			}

			rowsAffected = sqlDB.update(EventDatabase.TABLE_QUAKES, values,
					modSelection.toString(), null);
			break;
		case QUAKES:
			rowsAffected = sqlDB.update(EventDatabase.TABLE_QUAKES, values,
					selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI");
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

}
