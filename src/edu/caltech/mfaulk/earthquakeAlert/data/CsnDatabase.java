package edu.caltech.mfaulk.earthquakeUltra.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * CSN data includes 'quakes' containing event-level details like time and
 * magnitude, and a set of 'picks' for each event.
 * 
 * @author mfaulk
 * 
 */
public class CsnDatabase extends SQLiteOpenHelper {

	public static final String DEBUG_TAG = "CsnDatabase";
	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "csn_data";

	// Schema: Quakes
	public static final String TABLE_QUAKES = "csn_quakes";
	public static final String COL_QUAKE_ID = "_id";
	public static final String COL_QUAKE_TITLE = "title";
	public static final String COL_QUAKE_DESC = "description";
	public static final String COL_QUAKE_MAG = "mag";
	public static final String COL_QUAKE_URL = "url";
	// Unix time in seconds
	public static final String COL_QUAKE_DATE = "date";

	// Decimal Degrees
	public static final String COL_QUAKE_LAT = "quake_lat";
	public static final String COL_QUAKE_LON = "quake_lon";

	// Picks
	public static final String TABLE_PICKS = "csn_picks";
	public static final String COL_PICK_TITLE = "title";
	public static final String COL_PICK_ID = "_id";
	// Foreign key into quakes table
	public static final String COL_QUAKE_FOREIGN = "quake_foreign";
	public static final String COL_PICK_LAT = "pick_lat";
	public static final String COL_PICK_LON = "pick_lon";
	// Unix time in seconds
	public static final String COL_PICK_DATE = "pick_date";
	public static final String COL_PICK_MAG = "pick_mag";

	private static final String CREATE_EVENTS_TABLE = "create table "
			+ TABLE_QUAKES + " (" + COL_QUAKE_ID
			+ " integer primary key autoincrement, " + COL_QUAKE_URL
			+ " text unique not null, " + COL_QUAKE_TITLE + " text, "
			+ COL_QUAKE_MAG + " real, " + COL_QUAKE_DESC + " text, "
			+ COL_QUAKE_LAT + " real, " + COL_QUAKE_LON + " real, "
			+ COL_QUAKE_DATE + " integer not null " + ");";

	private static final String CREATE_PICKS_TABLE = "create table "
			+ TABLE_PICKS + " (" + COL_PICK_ID
			+ " integer primary key autoincrement, " + COL_PICK_MAG + " real, "
			+ COL_PICK_LAT + " real, " + COL_PICK_LON + " real, "
			+ COL_PICK_DATE + " integer not null, " + COL_QUAKE_FOREIGN
			+ " integer not null " + ");";

	public static ContentValues createPickContentValues(double lat, double lon,
			double mag, long date, long quakeID) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL_PICK_DATE, date);
		contentValues.put(COL_PICK_LAT, lat);
		contentValues.put(COL_PICK_LON, lon);
		contentValues.put(COL_PICK_MAG, mag);
		contentValues.put(COL_QUAKE_FOREIGN, quakeID);
		return contentValues;

	}

	public CsnDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_PICKS_TABLE);
		db.execSQL(CREATE_EVENTS_TABLE);
		
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DEBUG_TAG,
				"Upgrading database. Existing contents will be lost. ["
						+ oldVersion + "]->[" + newVersion + "]");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUAKES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PICKS);
		onCreate(db);
	}

}
