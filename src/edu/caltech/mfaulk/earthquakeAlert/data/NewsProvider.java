package edu.caltech.mfaulk.earthquakeUltra.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class NewsProvider extends ContentProvider {
	private static final String DEBUG_TAG = "NewsProvider";
	private static final String AUTHORITY = "edu.caltech.mfaulk.earthquakeUltra.data.NewsProvider";
	// Should this be set to a constant in NewsDatabase? e.g. the table name? Or
	// is this just for the URI Matcher?
	private static final String NEWS_BASE_PATH = "news";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + NEWS_BASE_PATH);

	// Codes returned by UriMatcher
	public static final int NEWS = 100;
	public static final int NEWS_BY_ID = 110;

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, NEWS_BASE_PATH, NEWS);
		sURIMatcher.addURI(AUTHORITY, NEWS_BASE_PATH + "/#", NEWS_BY_ID);
	}

	// MIME Types
	// TODO: replace mt-tutorial with something meaningful
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/mt-tutorial";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/mt-tutorial";

	private NewsDatabase mDB;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
		case NEWS:
			rowsAffected = sqlDB.delete(NewsDatabase.TABLE_NEWS, selection,
					selectionArgs);
			break;
		case NEWS_BY_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(NewsDatabase.TABLE_NEWS,
						NewsDatabase.ID + "=" + id, null);
			} else {
				rowsAffected = sqlDB.delete(NewsDatabase.TABLE_NEWS, selection
						+ " and " + NewsDatabase.ID + "=" + id, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case NEWS:
			return CONTENT_TYPE;
		case NEWS_BY_ID:
			return CONTENT_ITEM_TYPE;
		default:
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		if (uriType != NEWS) {
			throw new IllegalArgumentException("Invalid URI for insert");
		}
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		try {
			long newID = sqlDB.insertOrThrow(NewsDatabase.TABLE_NEWS, null,
					values);
			if (newID > 0) {
				Uri newUri = ContentUris.withAppendedId(uri, newID);
				getContext().getContentResolver().notifyChange(uri, null);
				return newUri;
			} else {
				throw new SQLException("Failed to insert row into " + uri);
			}
		} catch (SQLiteConstraintException e) {
			Log.i(DEBUG_TAG, "Ignoring constraint failure.");
		}
		return null;
	}

	@Override
	public boolean onCreate() {
		mDB = new NewsDatabase(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(NewsDatabase.TABLE_NEWS);
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case NEWS_BY_ID:
			queryBuilder.appendWhere(NewsDatabase.ID + "="
					+ uri.getLastPathSegment());
			break;
		case NEWS:
			// no extra filter
			break;
		default:
			throw new IllegalArgumentException("Unknown URI");
		}
		Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
				projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();

		int rowsAffected;

		switch (uriType) {
		case NEWS_BY_ID:
			String id = uri.getLastPathSegment();
			StringBuilder modSelection = new StringBuilder(NewsDatabase.ID
					+ "=" + id);

			if (!TextUtils.isEmpty(selection)) {
				modSelection.append(" AND " + selection);
			}

			rowsAffected = sqlDB.update(NewsDatabase.TABLE_NEWS, values,
					modSelection.toString(), null);
			break;
		case NEWS:
			rowsAffected = sqlDB.update(NewsDatabase.TABLE_NEWS, values,
					selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI");
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	/**
	 * Helper to mark all items (tutorials) in the table as read
	 * 
	 * @param context
	 *            A valid context
	 */
	// public static void markAllItemsRead(Context context) {
	// ContentValues values = new ContentValues();
	// values.put(TutListDatabase.COL_READ, "1");
	// int updated = context.getContentResolver().update(CONTENT_URI, values,
	// TutListDatabase.COL_READ + "='0'", null);
	// Log.d(DEBUG_TAG, "Rows updated: " + updated);
	// }

	/**
	 * Marks a single item, referenced by Uri, as read
	 * 
	 * @param context
	 *            A valid context
	 * @param item
	 *            An individual item
	 */
	// public static void markItemRead(Context context, long item) {
	// Uri viewedTut = Uri.withAppendedPath(TutListProvider.CONTENT_URI,
	// String.valueOf(item));
	// ContentValues values = new ContentValues();
	// values.put(TutListDatabase.COL_READ, "1");
	// int updated = context.getContentResolver().update(viewedTut, values,
	// null, null);
	// Log.d(DEBUG_TAG, updated + " rows updated. Marked " + item
	// + " as read.");
	// }

}
