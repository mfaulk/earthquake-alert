package edu.caltech.mfaulk.earthquakeUltra.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class CsnProvider extends ContentProvider {
	private static final String DEBUG_TAG = "CsnProvider";
	private static final String AUTHORITY = "edu.caltech.mfaulk.earthquakeUltra.data.CsnProvider";

	private static final String EVENTS_BASE_PATH = "events";
	public static final Uri EVENTS_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + EVENTS_BASE_PATH);
	
	private static final String PICKS_BASE_PATH = "picks";
	public static final Uri PICKS_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + PICKS_BASE_PATH);

	// Codes returned by UriMatcher
	public static final int QUAKES = 100;
	public static final int QUAKES_BY_ID = 110;
	public static final int PICKS = 200;

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, EVENTS_BASE_PATH, QUAKES);
		sURIMatcher.addURI(AUTHORITY, EVENTS_BASE_PATH + "/#", QUAKES_BY_ID);
		sURIMatcher.addURI(AUTHORITY, PICKS_BASE_PATH, PICKS);
	}

	// MIME Types
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/quake-events";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/quake-events";

	private CsnDatabase mDB;

	@Override
	public boolean onCreate() {
		mDB = new CsnDatabase(getContext());
		return true;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
		case QUAKES:
			rowsAffected = sqlDB.delete(CsnDatabase.TABLE_QUAKES, selection,
					selectionArgs);
			break;
		case QUAKES_BY_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(CsnDatabase.TABLE_QUAKES,
						CsnDatabase.COL_QUAKE_ID + "=" + id, null);
			} else {
				rowsAffected = sqlDB.delete(CsnDatabase.TABLE_QUAKES, selection
						+ " and " + CsnDatabase.COL_QUAKE_ID + "=" + id,
						selectionArgs);
			}
			break;
		case PICKS:
			Log.d(DEBUG_TAG, "TODO: delete picks");
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case PICKS:
			return CONTENT_TYPE;
		case QUAKES:
			return CONTENT_TYPE;
		case QUAKES_BY_ID:
			return CONTENT_ITEM_TYPE;
		default:
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		//Log.d(DEBUG_TAG, "INSERT");
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int uriType = sURIMatcher.match(uri);
		Uri newUri = null;
		switch (uriType) {
		case QUAKES:
			try {
				long newID = sqlDB.insertOrThrow(CsnDatabase.TABLE_QUAKES,
						null, values);
				if (newID > 0) {
					newUri = ContentUris.withAppendedId(uri, newID);
					getContext().getContentResolver().notifyChange(uri, null);
				} else {
					throw new SQLException("Failed to insert row into " + uri);
				}
			} catch (SQLiteConstraintException e) {
				Log.i(DEBUG_TAG, "Ignoring constraint failure.");
			}
			break;
		case PICKS:
			long newID = sqlDB.insertOrThrow(CsnDatabase.TABLE_PICKS, null,
					values);
			if (newID > 0) {
				newUri = ContentUris.withAppendedId(uri, newID);
				getContext().getContentResolver().notifyChange(uri, null);

			} else {
				throw new SQLException("Failed to insert row into " + uri);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		return newUri;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case QUAKES_BY_ID:
			queryBuilder.setTables(CsnDatabase.TABLE_QUAKES);
			queryBuilder.appendWhere(CsnDatabase.COL_QUAKE_ID + "="
					+ uri.getLastPathSegment());
			break;
		case QUAKES:
			queryBuilder.setTables(CsnDatabase.TABLE_QUAKES);
			// no extra filter
			break;
		case PICKS:
			queryBuilder.setTables(CsnDatabase.TABLE_PICKS);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI. uriType "
					+ Integer.toString(uriType));
		}
		Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
				projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();

		int rowsAffected = 0;

		switch (uriType) {
		case QUAKES_BY_ID:
			String id = uri.getLastPathSegment();
			StringBuilder modSelection = new StringBuilder(
					CsnDatabase.COL_QUAKE_ID + "=" + id);

			if (!TextUtils.isEmpty(selection)) {
				modSelection.append(" AND " + selection);
			}

			rowsAffected = sqlDB.update(CsnDatabase.TABLE_QUAKES, values,
					modSelection.toString(), null);
			break;
		case QUAKES:
			rowsAffected = sqlDB.update(CsnDatabase.TABLE_PICKS, values,
					selection, selectionArgs);
			break;
		case PICKS:
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI");
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}
}
