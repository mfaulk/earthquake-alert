package edu.caltech.mfaulk.earthquakeUltra.events;

import java.text.DateFormat;
import java.util.Date;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.TabActivity;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.commonsware.cwac.merge.MergeAdapter;

import edu.caltech.mfaulk.earthquakeUltra.R;
import edu.caltech.mfaulk.earthquakeUltra.SharedState;
import edu.caltech.mfaulk.earthquakeUltra.data.CsnDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.CsnProvider;
import edu.caltech.mfaulk.earthquakeUltra.data.EventDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.EventProvider;

public class EventsFragment extends ListFragment implements
		LoaderManager.LoaderCallbacks<Cursor> {
	private static final String DEBUG_TAG = "EventsFragment";
	// Loader IDs
	private static final int EVENT_LIST_LOADER_A = 0x01;
	private static final int EVENT_LIST_LOADER_B = 0x02;

	private MergeAdapter mergeAdapter;
	private SimpleCursorAdapter adapterA;
	private SimpleCursorAdapter adapterB;

	public interface OnEventSelectedListener {
		public void onEventSelected(String newsUrl);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		{
			String[] uiBindFrom = { CsnDatabase.COL_QUAKE_TITLE,
					CsnDatabase.COL_QUAKE_DESC };
			int[] uiBindTo = { R.id.txtTitle, R.id.txtSecondary };

			getLoaderManager().initLoader(EVENT_LIST_LOADER_A, null, this);
			adapterA = new SimpleCursorAdapter(getActivity()
					.getApplicationContext(), R.layout.csn_row, null,
					uiBindFrom, uiBindTo,
					CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
			adapterA.setViewBinder(new CsnViewBinder());
		}

		{
			String[] uiBindFrom = { EventDatabase.COL_QUAKE_TITLE,
					EventDatabase.COL_QUAKE_DESC, EventDatabase.COL_QUAKE_NET };
			int[] uiBindTo = { R.id.txtTitle, R.id.txtSecondary,
					R.id.eventImgIcon };

			getLoaderManager().initLoader(EVENT_LIST_LOADER_B, null, this);
			adapterB = new SimpleCursorAdapter(getActivity()
					.getApplicationContext(), R.layout.event_row_fancy, null,
					uiBindFrom, uiBindTo,
					CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
			adapterB.setViewBinder(new UsgsViewBinder());
		}
		mergeAdapter = new MergeAdapter();
		mergeAdapter.addAdapter(adapterA);
		mergeAdapter.addAdapter(adapterB);
		setListAdapter(mergeAdapter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		// TODO: handle item clicks separately for each member adapter.
		Log.d(DEBUG_TAG, "Clicked position: " + Integer.toString(position));

		int index = mergeAdapter.getPieceIndex(position);
		if (index == 0) {
			// adapterA : CSN events
			SimpleCursorAdapter baseAdapter = (SimpleCursorAdapter) mergeAdapter
					.getAdapter(position);
			int offset = mergeAdapter.getPositionInPiece(position);
			Cursor c = baseAdapter.getCursor();
			c.moveToPosition(offset);
			long quakeID = c.getInt(0);

			// Pass info to another Tab via SharedState.
			SharedState sharedState = SharedState.getInstance();
			sharedState.setVisualizationID(quakeID);
			((TabActivity) getActivity().getParent()).getTabHost()
					.setCurrentTab(1);
		} else if (index == 1) {
			// adapterB : USGS events
			Log.d(DEBUG_TAG, "Clicked in adapterB ");
			SimpleCursorAdapter baseAdapter = (SimpleCursorAdapter) mergeAdapter
					.getAdapter(position);
			int offset = mergeAdapter.getPositionInPiece(position);
			Cursor c = baseAdapter.getCursor();
			c.moveToPosition(offset);
			long quakeID = c.getInt(0);

			// Pass info to another Tab via SharedState.
			SharedState sharedState = SharedState.getInstance();
			sharedState.setVisualizationID(null);
			sharedState.setQuakeID(quakeID);
			((TabActivity) getActivity().getParent()).getTabHost()
					.setCurrentTab(1);
		}

	}

	private class CsnViewBinder implements SimpleCursorAdapter.ViewBinder {

		@Override
		public boolean setViewValue(View view, Cursor cursor, int index) {
			if (index == cursor.getColumnIndex(CsnDatabase.COL_QUAKE_DATE)) {
				// get a locale based string for the date
				DateFormat formatter = android.text.format.DateFormat
						.getDateFormat(getActivity().getApplicationContext());
				long date = cursor.getLong(index);
				Date dateObj = new Date(date * 1000);
				((TextView) view).setText(formatter.format(dateObj));
				return true;
			} else {
				return false;
			}
		}
	}

	private class UsgsViewBinder implements SimpleCursorAdapter.ViewBinder {
		@Override
		public boolean setViewValue(View view, Cursor cursor, int index) {
			if (index == cursor.getColumnIndex(EventDatabase.COL_QUAKE_DATE)) {
				// get a locale based string for the date
				DateFormat formatter = android.text.format.DateFormat
						.getDateFormat(getActivity().getApplicationContext());
				long date = cursor.getLong(index);
				Date dateObj = new Date(date * 1000);
				((TextView) view).setText(formatter.format(dateObj));
				return true;
			} else if (index == cursor
					.getColumnIndex(EventDatabase.COL_QUAKE_NET)) {
				if (cursor.getString(index).equalsIgnoreCase("USGS")) {
					// USGS icon is default
					((ImageView) view).setImageDrawable(getResources()
							.getDrawable(R.drawable.usgs_icon));
				} else if (cursor.getString(index).equalsIgnoreCase("CSN")) {
					((ImageView) view).setImageDrawable(getResources()
							.getDrawable(R.drawable.csn_icon));
				}
				return true;
			} else {
				return false;
			}
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int loaderID, Bundle arg1) {
		if (loaderID == EVENT_LIST_LOADER_A) {
			Log.d(DEBUG_TAG, "onCreateLoader, loader A");
			String[] projection = { CsnDatabase.COL_QUAKE_ID,
					CsnDatabase.COL_QUAKE_TITLE, CsnDatabase.COL_QUAKE_DESC };
			return new CursorLoader(getActivity(), CsnProvider.EVENTS_URI,
					projection, null, null, CsnDatabase.COL_QUAKE_DATE
							+ " desc");
		} else {
			Log.d(DEBUG_TAG, "onCreateLoader, loader B");
			String[] projection = { EventDatabase.ID,
					EventDatabase.COL_QUAKE_TITLE,
					EventDatabase.COL_QUAKE_DESC, EventDatabase.COL_QUAKE_NET };
			return new CursorLoader(getActivity(), EventProvider.CONTENT_URI,
					projection, null, null, EventDatabase.COL_QUAKE_DATE
							+ " desc");
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		int loaderID = loader.getId();
		Log.d(DEBUG_TAG,
				"onLoadFinished, Loader ID is: "
						+ Integer.toString(loader.getId()));
		if (loaderID == EVENT_LIST_LOADER_A) {
			Log.d(DEBUG_TAG, "onCreateLoader, loader A");
			adapterA.swapCursor(cursor);
		} else {
			Log.d(DEBUG_TAG, "onCreateLoader, loader B");
			adapterB.swapCursor(cursor);
		}

	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		Log.d(DEBUG_TAG,
				"onLoaderReset, Loader ID is: "
						+ Integer.toString(loader.getId()));
		int loaderID = loader.getId();
		if (loaderID == EVENT_LIST_LOADER_A) {
			Log.d(DEBUG_TAG, "onCreateLoader, loader A");
			adapterA.swapCursor(null);
		} else {
			Log.d(DEBUG_TAG, "onCreateLoader, loader B");
			adapterB.swapCursor(null);
		}
	}
}
