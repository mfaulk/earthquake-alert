package edu.caltech.mfaulk.earthquakeUltra.events;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import edu.caltech.mfaulk.earthquakeUltra.R;

public class EventsList extends Activity {
	static final String TAG = "EventsList";
	// TODO: the minimum quake size could be a user-specified "filter"
	// parameter.

	static final String QUAKE_RSS = "http://earthquake.usgs.gov/eqcenter/catalogs/1day-M2.5.xml";

	private ListView listView1;
	private ProgressDialog pDialog;
	ArrayList<EventItem> items = new ArrayList<EventItem>();
	EventAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.events_list);

		adapter = new EventAdapter(this, R.layout.event_row_fancy,
				items);
		listView1 = (ListView) findViewById(R.id.listView1);

		listView1.setAdapter(adapter);
		new EarthquakeLookupTask().execute();
		
		// launch an async task to fetch today's quakes

		// add an onclicklistener to the list. Maybe a click should change the
		// list item from minimal (magnitude, loaction, time) to a detailed view
		// with additional options?
		
	}
	

	private class EarthquakeLookupTask extends AsyncTask<Void, Void, List<EventItem>> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EventsList.this);
			pDialog.setMessage("Loading earthquake events...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected List<EventItem> doInBackground(Void... params) {
			List<EventItem> newEvents = new ArrayList<EventItem>();
			URL url;
			try {
				//String quakeFeed = getString(R.string.quake_feed);
				url = new URL(QUAKE_RSS);
				URLConnection connection;
				connection = url.openConnection();
				HttpURLConnection httpConnection = (HttpURLConnection) connection;
				int responseCode = httpConnection.getResponseCode();
				if (responseCode == HttpURLConnection.HTTP_OK) {
					InputStream in = httpConnection.getInputStream();
					DocumentBuilderFactory dbf = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder db = dbf.newDocumentBuilder();
					// Parse the earthquake feed.
					Document dom = db.parse(in);
					Element docEle = dom.getDocumentElement();

					// Get a list of each earthquake entry.
					NodeList nl = docEle.getElementsByTagName("entry");
					if (nl != null && nl.getLength() > 0) {
						for (int i = 0; i < nl.getLength(); i++) {
							Element entry = (Element) nl.item(i);
							Element title = (Element) entry
									.getElementsByTagName("title").item(0);
							Element g = (Element) entry.getElementsByTagName(
									"georss:point").item(0);
							Element when = (Element) entry
									.getElementsByTagName("updated").item(0);
							Element link = (Element) entry
									.getElementsByTagName("link").item(0);

							String details = title.getFirstChild()
									.getNodeValue();
							String hostname = "http://earthquake.usgs.gov";
							String linkString = hostname
									+ link.getAttribute("href");
							String point = g.getFirstChild().getNodeValue();
							String dt = when.getFirstChild().getNodeValue();

							// date
							SimpleDateFormat sdf = new SimpleDateFormat(
									"yyyy-MM-dd'T'hh:mm:ss'Z'");
							Date qdate = new GregorianCalendar(0, 0, 0)
									.getTime();
							try {
								qdate = sdf.parse(dt);
							} catch (java.text.ParseException e) {
								e.printStackTrace();
							}

							// location
							String[] location = point.split(" ");
							Location l = new Location("dummyGPS");

							final double latitude = Double
									.parseDouble(location[0]);
							final double longitude = Double
									.parseDouble(location[1]);

							l.setLatitude(latitude);
							l.setLongitude(longitude);

							// magnitude
							String magnitudeString = details.split(" ")[1];
							int end = magnitudeString.length() - 1;
							final double magnitude = Double
									.parseDouble(magnitudeString.substring(0,
											end));

							// details
							details = details.split(",")[1].trim();
//							Quake quake = new Quake(qdate, details, l,
//									magnitude, linkString);
							UsgsQuakeEvent event = new UsgsQuakeEvent(Double.toString(magnitude), qdate.toString());

							newEvents.add(event);
						}
					}

				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} finally {
			}

			return newEvents;
		}

		@Override
		protected void onPostExecute(List<EventItem> result) {
			adapter.setList(result);
		      adapter.notifyDataSetChanged();
		      pDialog.dismiss();
		}
		
	}

	/**
	 * Background Async Task to get quakes from USGS RSS feed
	 * */
	class LoadUSGSQuakes extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EventsList.this);
			pDialog.setMessage("Loading earthquake events...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting all recent articles and showing them in listview
		 * */
		@Override
		protected String doInBackground(String... args) {
			// rss link url
			String rss_url = args[0];
			
			

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					
				}
			});
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String args) {
			pDialog.dismiss();
		}
	}

}
