package edu.caltech.mfaulk.earthquakeUltra.events;

import edu.caltech.mfaulk.earthquakeUltra.R;

/**
 * Represents an element of the USGS quake RSS feed, e.g. 
 * http://earthquake.usgs.gov/eqcenter/catalogs/1day-M2.5.xml
 * 
 * TODO: 
 *   implement Comparable<EventItem> for easy sorting by date (or magnitude?)
 *   use this with JSoup for parsing the RSS feed?
 * @author mfaulk
 *
 */
public class UsgsQuakeEvent implements EventItem {
	private String primaryText;
	private String secondaryText;
	
	public UsgsQuakeEvent(String primaryText, String secondaryText) {
		this.primaryText = primaryText;
		this.secondaryText = secondaryText;
	}
	
	
	public void setPrimaryText(String primaryText) {
		this.primaryText = primaryText;
	}

	public void setSecondaryText(String secondaryText) {
		this.secondaryText = secondaryText;
	}

	@Override
	public String getPrimaryText() {
		return primaryText;
	}

	@Override
	public String getSecondaryText() {
		return secondaryText;
	}
	
	@Override
	public int getImageId() {
		return R.drawable.usgs_icon;
	}

}
