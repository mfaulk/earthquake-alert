package edu.caltech.mfaulk.earthquakeUltra.events;

/**
 * A pick reported to the CSN system. To be used in conjunction with CsnEvent
 * @author mfaulk
 *
 */
public class CsnPick {

	public long id;
	public double latitude;
	public double longitude;
	public double offsetSeconds;
	
	// GSON requires a no-argument constructor.
	public CsnPick(){}
}
