package edu.caltech.mfaulk.earthquakeUltra.events;

public interface EventItem {
	public String getPrimaryText();
	public String getSecondaryText();
	public int getImageId();
	// public View getView();
}
