package edu.caltech.mfaulk.earthquakeUltra.events;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import edu.caltech.mfaulk.earthquakeUltra.R;
import edu.caltech.mfaulk.earthquakeUltra.service.EventDownloaderService;

public class EventsActivity extends Activity implements EventsFragment.OnEventSelectedListener{
	private static final String DEBUG_TAG = "EventsActivity";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.events_fragment);
	}

	@Override
	public void onEventSelected(String eventUrl) {
		Log.d(DEBUG_TAG, "onEventSelected: " + eventUrl);
	}

	@Override
	public boolean onPreparePanel(int featureId, View view, Menu menu) {
		// TODO should this be in onCreatePanel?
		Log.d(DEBUG_TAG, "onPreparePanelMenu");
		Intent intent = new Intent(this.getApplicationContext(),
				EventDownloaderService.class);
		MenuItem refresh = menu.findItem(R.id.refresh_option_item);
		refresh.setIntent(intent);
		return super.onPreparePanel(featureId, view, menu);
	}

	@Override
	public boolean onCreatePanelMenu(int arg0, Menu arg1) {
		Log.d(DEBUG_TAG, "onCreatePanelMenu");
		return super.onCreatePanelMenu(arg0, arg1);
	}

	
}
