package edu.caltech.mfaulk.earthquakeUltra.events;

import java.util.ArrayList;

/**
 * An event detected by the CSN system
 * @author mfaulk
 *
 */
public class CsnEvent {
	public long id;
	public String name;
	public ArrayList<CsnPick> picks;
	
	// GSON requires a no-argument constructor
	public CsnEvent() {}
}
