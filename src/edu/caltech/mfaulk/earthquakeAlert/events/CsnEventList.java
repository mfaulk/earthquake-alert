package edu.caltech.mfaulk.earthquakeUltra.events;

import java.util.ArrayList;

public class CsnEventList {
	public ArrayList<CsnEvent> events;
	
	// GSON requires a no-argument constructor
	public CsnEventList(){};
}
