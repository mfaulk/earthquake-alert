package edu.caltech.mfaulk.earthquakeUltra.events;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import edu.caltech.mfaulk.earthquakeUltra.R;

public class EventAdapter extends ArrayAdapter<EventItem> {
	static final String TAG = "EventAdapter";

	private Context context;
	private int layoutResourceId;
	ArrayList<EventItem> data = new ArrayList<EventItem>();
	
	public EventAdapter(Context context, int textViewResourceId,
			ArrayList<EventItem> data) {
		super(context, textViewResourceId, data);
		this.layoutResourceId = textViewResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ContentHolder holder = null;
		if(row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			holder = new ContentHolder();
			holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
			holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
			holder.txtSecondary = (TextView)row.findViewById(R.id.txtSecondary);
			// I think setTag is used as a cheap way to store some data with the row. mf.
			row.setTag(holder);
		} else {
			holder = (ContentHolder)row.getTag();
		}
		EventItem content = data.get(position);
		holder.txtTitle.setText(content.getPrimaryText());
		holder.txtSecondary.setText(content.getSecondaryText());
		holder.imgIcon.setImageResource(content.getImageId());;
		return row;
	}

	static class ContentHolder{
		ImageView imgIcon;
		TextView txtTitle;
		TextView txtSecondary;
	}
	
	void setList(List<EventItem> events) {
		data.clear();
		data.addAll(events);
	}
}
