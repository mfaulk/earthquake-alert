package edu.caltech.mfaulk.earthquakeUltra;

public class SharedState {
	static final String TAG = "SharedState";
	private static SharedState instance = null;
	
	// ID into the EventDatabase
	private Long quakeID = null;
	
	private Long visualizationID = null;
	
	private String newsUrl = null;
	
	private SharedState(){}
	
	public static SharedState getInstance() {
		if (instance == null) {
			instance = new SharedState();
		}
		return instance;
	}
	
	public void setNewsUrl(String url) {
		newsUrl = url;
	}
	
	public String getNewsUrl() {
		return newsUrl;
	}
	
	public void setQuakeID(Long id) {
		quakeID = id;
	}
	
	/**
	 * 
	 * @return ID into the EventDatabase. May be null.
	 */
	public Long getQuakeID() {
		return quakeID;
	}
	
	
	/**
	 * MAY BE UNIMPLEMENTED
	 * @return ID into the (?)Database. May be null.
	 */
	public Long getVisualizationID() {
		return visualizationID;
	}

	public void setVisualizationID(Long visualizationID) {
		this.visualizationID = visualizationID;
	}

}
