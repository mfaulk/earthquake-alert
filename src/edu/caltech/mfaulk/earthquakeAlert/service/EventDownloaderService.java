package edu.caltech.mfaulk.earthquakeUltra.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.jsoup.Jsoup;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;

import edu.caltech.mfaulk.earthquakeUltra.data.CsnDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.CsnProvider;
import edu.caltech.mfaulk.earthquakeUltra.data.EventDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.EventProvider;
import edu.caltech.mfaulk.earthquakeUltra.events.CsnEvent;
import edu.caltech.mfaulk.earthquakeUltra.events.CsnEventList;
import edu.caltech.mfaulk.earthquakeUltra.events.CsnPick;

public class EventDownloaderService extends Service {
	private static final String DEBUG_TAG = "EventDownloaderService";
	// TODO: move this into resources or preferences
	static final String QUAKE_RSS = "http://earthquake.usgs.gov/eqcenter/catalogs/1day-M2.5.xml";
	static final String CSN_EVENT_JSON = "http://pure-citadel-7866.herokuapp.com/events";
	// static final String QUAKE_RSS =
	// "http://earthquake.usgs.gov/eqcenter/catalogs/7day-M2.5.xml";
	private UsgsDownloaderTask usgsDownloader;
	private CsnDownloaderTask csnDownloader;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			// Delete old events
			System.setProperty("org.joda.time.DateTimeZone.Provider",
					"org.joda.time.tz.UTCProvider");
			usgsDownloader = new UsgsDownloaderTask();
			usgsDownloader.execute(new URL(QUAKE_RSS));
			csnDownloader = new CsnDownloaderTask();
			csnDownloader.execute(new URL(CSN_EVENT_JSON));
		} catch (Exception e) {
			Log.e(DEBUG_TAG, "Bad URL", e);
		}

		return Service.START_FLAG_REDELIVERY;
	}

	private void purgeOldEvents() {
		DateTime now = new DateTime();
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Log.d(DEBUG_TAG, "It is now: " + fmt.print(now));

		DateTime cutOff = now.minusDays(1);
		// DateTime cutOff = now;
		String cutOffSeconds = Long.toString(cutOff.getMillis() / 1000);
		Log.d(DEBUG_TAG, "Cutoff seconds: " + cutOffSeconds);

		int nUsgsDeleted = getContentResolver().delete(
				EventProvider.CONTENT_URI,
				EventDatabase.COL_QUAKE_DATE + "<" + cutOffSeconds, null);
		Log.d(DEBUG_TAG, "Deleted " + Integer.toString(nUsgsDeleted)
				+ " old USGS events.");

		int nCsnDeleted = getContentResolver().delete(CsnProvider.EVENTS_URI,
				CsnDatabase.COL_QUAKE_DATE + "<" + cutOffSeconds, null);
		Log.d(DEBUG_TAG, "Deleted " + Integer.toString(nCsnDeleted)
				+ " old CSN events.");

	}

	private class CsnDownloaderTask extends AsyncTask<URL, Void, Boolean> {
		private static final String DEBUG_TAG = "EventDownloaderService$CsnDownloaderTask";

		@Override
		protected Boolean doInBackground(URL... params) {

			boolean succeeded = false;
			URL downloadPath = params[0];
			if (downloadPath != null) {
				succeeded = jsonParse(downloadPath);
			}
			return succeeded;
		}

		private Boolean jsonParse(URL url) {
			boolean success = false;
			InputStream is;
			URLConnection urlConnection;
			try {
				urlConnection = url.openConnection();
				urlConnection.setConnectTimeout(1000);
				is = urlConnection.getInputStream();
				String json = getStringFromInputStream(is);
				Log.d(DEBUG_TAG, "JSON data:" + json);
				Gson gson = new Gson();
				CsnEventList csnEventList = gson.fromJson(json,
						CsnEventList.class);
				if (csnEventList != null) {
					Log.d(DEBUG_TAG, "Downloaded " + csnEventList.events.size()
							+ " CSN events.");
					for (CsnEvent csnEvent : csnEventList.events) {
						persistCsnEvent(csnEvent);
					}
					success = true;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// ContentValues eventData = new ContentValues();
			// populate content values

			// DateTime eventDateTime = new DateTime();
			// Long dateSeconds = eventDateTime.getMillis() / 1000L;
			// eventData.put(CsnDatabase.COL_QUAKE_DATE, dateSeconds);
			// eventData.put(CsnDatabase.COL_QUAKE_DESC, "A CSN event!");
			// eventData.put(CsnDatabase.COL_QUAKE_LAT, 34.14003);
			// eventData.put(CsnDatabase.COL_QUAKE_LON, -118.09832);
			// eventData.put(CsnDatabase.COL_QUAKE_MAG, 0.0);
			// eventData.put(CsnDatabase.COL_QUAKE_TITLE, "sample CSN quake.");
			// eventData.put(CsnDatabase.COL_QUAKE_URL, "www.google.com");

			// Insert may return null if a constraint (e.g. a unique field) is
			// violated.
			// Uri newUri = getContentResolver().insert(CsnProvider.EVENTS_URI,
			// eventData);
			// if(newUri != null) {
			// try {
			// int quakeID = Integer.parseInt(newUri.getLastPathSegment());
			// // TODO: get real pick data
			// long pickSeconds = eventDateTime.plusSeconds(4).getMillis() /
			// 1000L;
			// ContentValues pickData = CsnDatabase.createPickContentValues(
			// 34.14003, -118.09832, 1.1, pickSeconds, quakeID);
			// getContentResolver().insert(CsnProvider.PICKS_URI, pickData);
			// } catch (NumberFormatException e) {
			// Log.e(DEBUG_TAG, e.getMessage());
			// }
			// Log.d(DEBUG_TAG,
			// "Inserted event has ID " + newUri.getLastPathSegment());
			// }

			return success;
		}

		private void persistCsnEvent(CsnEvent csnEvent) {
			ContentValues eventData = new ContentValues();
			// populate content values

			DateTime eventDateTime = new DateTime();
			Long dateSeconds = eventDateTime.getMillis() / 1000L;
			eventData.put(CsnDatabase.COL_QUAKE_DATE, dateSeconds);
			eventData.put(CsnDatabase.COL_QUAKE_DESC, csnEvent.name);
			// TODO:
			eventData.put(CsnDatabase.COL_QUAKE_LAT, 34.14003);
			eventData.put(CsnDatabase.COL_QUAKE_LON, -118.09832);
			eventData.put(CsnDatabase.COL_QUAKE_MAG, 0.0);
			eventData.put(CsnDatabase.COL_QUAKE_TITLE, csnEvent.name);
			eventData.put(CsnDatabase.COL_QUAKE_URL, "www.google.com/"
					+ csnEvent.name);

			Uri newUri = getContentResolver().insert(CsnProvider.EVENTS_URI,
					eventData);
			if (newUri != null) {
				try {
					int quakeID = Integer.parseInt(newUri.getLastPathSegment());
					for (CsnPick p : csnEvent.picks) {
						long pickSeconds = eventDateTime.plusSeconds(
								(int) p.offsetSeconds).getMillis() / 1000L;
						ContentValues pickData = CsnDatabase
								.createPickContentValues(p.latitude,
										p.longitude, 1.1, pickSeconds, quakeID);
						getContentResolver().insert(CsnProvider.PICKS_URI,
								pickData);
					}
					// long pickSeconds =
					// eventDateTime.plusSeconds(4).getMillis() /
					// 1000L;
					// ContentValues pickData =
					// CsnDatabase.createPickContentValues(
					// 34.14003, -118.09832, 1.1, pickSeconds, quakeID);
					// getContentResolver().insert(CsnProvider.PICKS_URI,
					// pickData);
				} catch (NumberFormatException e) {
					Log.e(DEBUG_TAG, e.getMessage());
				}
				// Log.d(DEBUG_TAG,
				// "Inserted event has ID " + newUri.getLastPathSegment());
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			purgeOldEvents();
		}

		private String getStringFromInputStream(InputStream is) {

			BufferedReader br = null;
			StringBuilder sb = new StringBuilder();

			String line;
			try {

				br = new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			return sb.toString();

		}
	}

	/*
	 * A quake item looks like: <entry>
	 * <id>urn:earthquake-usgs-gov:hv:60449321</id> <title>M 2.5, Island of
	 * Hawaii, Hawaii</title> <updated>2013-01-11T19:32:36Z</updated> <link
	 * rel="alternate" type="text/html" href=
	 * "http://earthquake.usgs.gov/earthquakes/recenteqsww/Quakes/hv60449321.php"
	 * /> <summary type="html"> <![CDATA[ <img
	 * src="http://earthquake.usgs.gov/images/globes/20_-155.jpg"
	 * alt="19.291&#176;N 155.213&#176;W" align="left" hspace="20" /><p>Friday,
	 * January 11, 2013 19:32:36 UTC<br>Friday, January 11, 2013 09:32:36 AM at
	 * epicenter</p><p><strong>Depth</strong>: 8.40 km (5.22 mi)</p> ]]>
	 * </summary> <georss:point>19.2905 -155.2125</georss:point>
	 * <georss:elev>-8400</georss:elev> <category label="Age" term="Past day"/>
	 * </entry>
	 * 
	 * or
	 * 
	 * <entry> <id>urn:earthquake-usgs-gov:us:c000evg1</id> <title>M 4.2, Bosnia
	 * and Herzegovina</title> <updated>2013-01-25T18:52:26Z</updated> <link
	 * rel="alternate" type="text/html" href=
	 * "http://earthquake.usgs.gov/earthquakes/recenteqsww/Quakes/usc000evg1.php"
	 * /> <link rel="related" type="application/cap+xml"
	 * href="http://earthquake.usgs.gov/earthquakes/catalogs/cap/usc000evg1"/>
	 * <summary type="html"> <![CDATA[ <img
	 * src="http://earthquake.usgs.gov/images/globes/45_20.jpg"
	 * alt="43.337&#176;N 18.765&#176;E" align="left" hspace="20" /><p>Friday,
	 * January 25, 2013 18:52:26 UTC<br>Friday, January 25, 2013 07:52:26 PM at
	 * epicenter</p><p><strong>Depth</strong>: 11.00 km (6.84 mi)</p> ]]>
	 * </summary> <georss:point>43.3369 18.7652</georss:point>
	 * <georss:elev>-11000</georss:elev> <category label="Age" term="Past day"/>
	 * </entry>
	 */
	private class UsgsDownloaderTask extends AsyncTask<URL, Void, Boolean> {
		private static final String DEBUG_TAG = "EventDownloaderService$UsgsDownloaderTask";

		@Override
		protected void onPostExecute(Boolean result) {
			purgeOldEvents();
		}

		@Override
		protected Boolean doInBackground(URL... params) {
			boolean succeeded = false;
			URL downloadPath = params[0];
			if (downloadPath != null) {
				succeeded = xmlParse(downloadPath);
			}
			return succeeded;
		}

		private boolean xmlParse(URL downloadPath) {
			boolean succeeded = false;

			XmlPullParser pullParser;

			try {

				pullParser = XmlPullParserFactory.newInstance().newPullParser();
				pullParser.setInput(downloadPath.openStream(), null);
				int eventType = -1;

				while (eventType != XmlPullParser.END_DOCUMENT) {
					if (eventType == XmlPullParser.START_TAG) {
						String tagName = pullParser.getName();
						if (tagName.equals("entry")) {
							ContentValues eventData = new ContentValues();
							// NETWORK
							eventData.put(EventDatabase.COL_QUAKE_NET, "USGS");
							while (eventType != XmlPullParser.END_DOCUMENT) {
								if (eventType == XmlPullParser.START_TAG) {
									if (pullParser.getName().equals("link")) {
										// LINK
										String url = pullParser
												.getAttributeValue(null, "href");
										String rel = pullParser
												.getAttributeValue(null, "rel");
										pullParser.next();

										// String url = pullParser.getText();
										if (rel.equalsIgnoreCase("alternate")) {
											Log.d(DEBUG_TAG, "Event url: "
													+ url);
											eventData
													.put(EventDatabase.COL_QUAKE_URL,
															url);
										}
									} else if (pullParser.getName().equals(
											"title")) {
										// TITLE and MAGNITUDE
										pullParser.next();
										String title = pullParser.getText();
										eventData.put(
												EventDatabase.COL_QUAKE_TITLE,
												title);
										String[] parts = title.split("(,|\\s)");
										Double magnitude = 0.0;
										if (parts.length > 1) {
											try {
												magnitude = Double
														.parseDouble(parts[1]);
											} catch (NumberFormatException e) {
												Log.e(DEBUG_TAG, e.getMessage());
											}
										} else {
											Log.d(DEBUG_TAG,
													"Failed parsing magnitude.");
										}
										eventData.put(
												EventDatabase.COL_QUAKE_MAG,
												magnitude);
									} else if (pullParser.getName().equals(
											"updated")) {
										// DATE
										pullParser.next();
										DateFormat inputFormate = new SimpleDateFormat(
												"yyyy-MM-dd'T'HH:mm:ss'Z'",
												Locale.US);
										try {
											Date date = inputFormate
													.parse(pullParser.getText());
											Long dateLong = (Long) (date
													.getTime() / 1000);
											Log.d(DEBUG_TAG, "Event time: "
													+ Long.toString(dateLong));
											// DateFormat outputFormat =
											// DateFormat.getDateTimeInstance();
											// eventData.put(EventDatabase.COL_QUAKE_DATE,
											// outputFormat.format(date));
											eventData
													.put(EventDatabase.COL_QUAKE_DATE,
															dateLong);
										} catch (java.text.ParseException e) {
											e.printStackTrace();
											Log.e(DEBUG_TAG,
													"Error parsing date: "
															+ pullParser
																	.getText());
										}
									} else if (pullParser.getName().equals(
											"summary")) {
										// DESCRIPTION
										pullParser.next();
										String descriptionHTML = pullParser
												.getText();
										eventData.put(
												EventDatabase.COL_QUAKE_DESC,
												Jsoup.parse(descriptionHTML)
														.text());
									} else if (pullParser.getName().equals(
											"georss:point")) {
										// LOCATION
										pullParser.next();
										String latLon = pullParser.getText();

										// split and parse into doubles
										String[] parts = latLon.split(" ");
										if (parts.length == 2) {
											double lat = Double
													.parseDouble(parts[0]);
											double lon = Double
													.parseDouble(parts[1]);
											eventData
													.put(EventDatabase.COL_QUAKE_LAT,
															lat);
											eventData
													.put(EventDatabase.COL_QUAKE_LON,
															lon);
										} else {
											Log.d(DEBUG_TAG,
													"Lat-Lon parsing failed!");
										}
									}

								} else if (eventType == XmlPullParser.END_TAG) {
									if (pullParser.getName().equals("entry")) {
										// save the data, and then continue with
										// the outer loop

										// Do not persist events with title
										// "Data Feed Deprecated"
										if (!eventData.getAsString(
												EventDatabase.COL_QUAKE_TITLE)
												.equalsIgnoreCase(
														"Data Feed Deprecated")) {
											getContentResolver().insert(
													EventProvider.CONTENT_URI,
													eventData);
										}
										break;
									}
								}
								eventType = pullParser.next();
							}
						}
					}
					eventType = pullParser.next();
				}
				// no exceptions during parsing
				succeeded = true;
			} catch (XmlPullParserException e) {
				Log.e(DEBUG_TAG, "Error during parsing", e);
			} catch (IOException e) {
				Log.e(DEBUG_TAG, "IO Error during parsing", e);
			}

			return succeeded;
		}
		// TODO
		// onPostExecute

	}
}
