package edu.caltech.mfaulk.earthquakeUltra.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import edu.caltech.mfaulk.earthquakeUltra.data.NewsDatabase;
import edu.caltech.mfaulk.earthquakeUltra.data.NewsProvider;

public class NewsDownloaderService extends Service {
	private static final String DEBUG_TAG = "NewsDownloaderService";
	private DownloaderTask newsDownloader;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		URL newsFeedPath;
		try {
			String url = intent.getDataString();
			if (url != null && (url.length() > 0)) {
				newsFeedPath = new URL(url);
				newsDownloader = new DownloaderTask();
				newsDownloader.execute(newsFeedPath);
			}
		} catch (MalformedURLException e) {
			Log.e(DEBUG_TAG, "Bad URL", e);
		}

		return Service.START_FLAG_REDELIVERY;
	}

	private class DownloaderTask extends AsyncTask<URL, Void, Boolean> {
		private static final String DEBUG_TAG = "NewsDownloaderService$DownloaderTask";

		@Override
		protected Boolean doInBackground(URL... params) {
			boolean succeeded = false;
			URL downloadPath = params[0];
			if (downloadPath != null) {
				succeeded = xmlParse(downloadPath);
			}
			return succeeded;
		}

		private boolean xmlParse(URL downloadPath) {
			boolean succeeded = false;
			XmlPullParser pullParser;
			try {
				pullParser = XmlPullParserFactory.newInstance().newPullParser();
				pullParser.setInput(downloadPath.openStream(), null);
				int eventType = -1;

				// for each found "item" tag, find "link" and "title" tags
				// before end tag "item"

				while (eventType != XmlPullParser.END_DOCUMENT) {
					if (eventType == XmlPullParser.START_TAG) {
						String tagName = pullParser.getName();
						if (tagName.equals("item")) {

							ContentValues newsData = new ContentValues();
							// inner loop looking for link and title
							while (eventType != XmlPullParser.END_DOCUMENT) {
								if (eventType == XmlPullParser.START_TAG) {
									if (pullParser.getName().equals("link")) {
										pullParser.next();
										newsData.put(NewsDatabase.COL_URL,
												pullParser.getText());
									} else if (pullParser.getName().equals(
											"title")) {
										pullParser.next();
										newsData.put(NewsDatabase.COL_TITLE,
												pullParser.getText());
									} else if (pullParser.getName().equals(
											"pubDate")) {
										pullParser.next();
											newsData.put(NewsDatabase.COL_DATE,
													pullParser.getText());
									} else if (pullParser.getName().equals(
											"description")) {
										pullParser.next();
										String descriptionHTML = pullParser.getText();
										newsData.put(NewsDatabase.COL_DESC,
												Jsoup.parse(descriptionHTML).text());
									}
								} else if (eventType == XmlPullParser.END_TAG) {
									if (pullParser.getName().equals("item")) {
										// save the data, and then continue with
										// the outer loop
										getContentResolver().insert(
												NewsProvider.CONTENT_URI,
												newsData);
										break;
									}
								}
								eventType = pullParser.next();
							}
						}
					}
					eventType = pullParser.next();
				}
				// no exceptions during parsing
				succeeded = true;
			} catch (XmlPullParserException e) {
				Log.e(DEBUG_TAG, "Error during parsing", e);
			} catch (IOException e) {
				Log.e(DEBUG_TAG, "IO Error during parsing", e);
			}

			return succeeded;
		}

		// TODO
		// onPostExecute

	}

	// /**
	// * Background Async Task to get RSS Feed Items data from URL
	// * */
	// class loadRSSFeedItems extends AsyncTask<String, String, String> {
	//
	// /**
	// * Before starting background thread Show Progress Dialog
	// * */
	// @Override
	// protected void onPreExecute() {
	// super.onPreExecute();
	// pDialog = new ProgressDialog(ListRSSItemsActivity.this);
	// pDialog.setMessage("Loading earthquake news...");
	// pDialog.setIndeterminate(false);
	// pDialog.setCancelable(false);
	// pDialog.show();
	// }
	//
	// /**
	// * getting all recent articles and showing them in listview
	// * */
	// @Override
	// protected String doInBackground(String... args) {
	// // rss link url
	// String rss_url = args[0];
	//
	// try {
	// rssItems = rssParser.getRSSFeedItems(rss_url);
	// // looping through each item
	// for (RSSItem item : rssItems) {
	// Log.d("LoadRSSFeedItems", " === item === ");
	// // creating new HashMap
	// HashMap<String, String> map = new HashMap<String, String>();
	//
	// // adding each child node to HashMap key => value
	// map.put(TAG_TITLE, item.getTitle());
	// map.put(TAG_LINK, item.getLink());
	// map.put(TAG_PUB_DATE, item.getPubdate());
	// String description = item.getDescription();
	// // taking only 200 chars from description
	// if (description.length() > 100) {
	// description = description.substring(0, 97) + "..";
	// }
	// map.put(TAG_DESRIPTION, description);
	//
	// // adding HashList to ArrayList
	// rssItemList.add(map);
	// }
	//
	// // updating UI from Background Thread
	// runOnUiThread(new Runnable() {
	// public void run() {
	// /**
	// * Updating parsed items into listview
	// * */
	// ListAdapter adapter = new SimpleAdapter(
	// ListRSSItemsActivity.this, rssItemList,
	// R.layout.rss_item_list_row, new String[] {
	// TAG_LINK, TAG_TITLE, TAG_PUB_DATE,
	// TAG_DESRIPTION }, new int[] {
	// R.id.page_url, R.id.title,
	// R.id.pub_date, R.id.link });
	//
	// // updating listview
	// setListAdapter(adapter);
	// }
	// });
	// } catch (Exception e) {
	// Log.d("LoadRSSFeedItems", Log.getStackTraceString(e));
	// }
	// return null;
	// }
	//
	// /**
	// * After completing background task Dismiss the progress dialog
	// * **/
	// protected void onPostExecute(String args) {
	// // dismiss the dialog after getting all products
	// pDialog.dismiss();
	// }
	// }

}
