package edu.caltech.mfaulk.earthquakeUltra;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import edu.caltech.mfaulk.earthquakeUltra.events.EventsActivity;
import edu.caltech.mfaulk.earthquakeUltra.map.QuakeMap;
import edu.caltech.mfaulk.earthquakeUltra.news.NewsActivity;
import edu.caltech.mfaulk.earthquakeUltra.service.EventDownloaderService;
import edu.caltech.mfaulk.earthquakeUltra.service.NewsDownloaderService;

public class NavigationActivity extends TabActivity {
	private final String DEBUG_TAG = "NavigationActivity";
	private TabHost mTabHost;
	private int refreshMenuId;
	
	
	private void setupTabHost() {
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// construct the tabhost
		setContentView(R.layout.main);

		setupTabHost();
		mTabHost.getTabWidget().setDividerDrawable(R.drawable.tab_divider);
		Intent eventsListIntent = new Intent(this, EventsActivity.class);
		setupTab(new TextView(this), "Latest", eventsListIntent);
		Intent mapIntent = new Intent(this, QuakeMap.class);
		setupTab(new TextView(this), "Map", mapIntent);
		Intent newsIntent = new Intent(this, NewsActivity.class);
		setupTab(new TextView(this), "News", newsIntent);
		
		// start background services
		Intent eventServiceIntent = new Intent(this.getApplicationContext(), EventDownloaderService.class);
		startService(eventServiceIntent);
		
		Intent newsServiceIntent = new Intent(this.getApplicationContext(),
				NewsDownloaderService.class);
		newsServiceIntent.setData(Uri
				.parse(NewsActivity.GOOGLE_NEWS_FEED));
		startService(newsServiceIntent);
	}

	private void setupTab(final View view, final String tag) {
		View tabview = createTabView(mTabHost.getContext(), tag);

		TabSpec setContent = mTabHost.newTabSpec(tag).setIndicator(tabview)
				.setContent(new TabContentFactory() {
					public View createTabContent(String tag) {
						return view;
					}
				});
		mTabHost.addTab(setContent);
	}

	private void setupTab(final View view, final String tag, Intent intent) {
		View tabview = createTabView(mTabHost.getContext(), tag);
		TabSpec setContent = mTabHost.newTabSpec(tag).setIndicator(tabview)
				.setContent(intent);
		mTabHost.addTab(setContent);
	}

	private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context)
				.inflate(R.layout.tabs_bg, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		return view;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.simple_menu, menu);
		MenuItem refresh = menu.findItem(R.id.refresh_option_item);
		refreshMenuId = refresh.getItemId();
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(DEBUG_TAG, "onOptionsItemSelected");
		if (item.getItemId() == refreshMenuId) {
			// Assumes that the individual tabs bind the refresh button to a service intent.
			if (item.getIntent() != null) {
				startService(item.getIntent());
			}
		}
		return true;
	}

}
